import React from 'react'
import {render} from 'react-dom'
import routes from './routes'

document.addEventListener("DOMContentLoaded", function() {
  render(routes, document.getElementById('app'))
});
