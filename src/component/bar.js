import React from 'react'
import { Link } from 'react-router'

class Bar extends React.Component {
  render() {
    return (
      <div>
        bar <Link to="/">foo</Link>
      </div>
    )
  }
}

export default Bar
