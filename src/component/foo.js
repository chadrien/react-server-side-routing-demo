import React from 'react'
import { Link } from 'react-router'

class Foo extends React.Component {
  render() {
    return (
      <div>
        foo <Link to="/bar">bar</Link>
      </div>
    )
  }
}

export default Foo
