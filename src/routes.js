import React from 'react'
import {Router, Route, browserHistory} from 'react-router'
import Foo from './component/foo'
import Bar from './component/bar'

export default (
  <Router history={browserHistory}>
    <Route path="/" component={Foo}/>
    <Route path="/bar" component={Bar}/>
  </Router>
)
