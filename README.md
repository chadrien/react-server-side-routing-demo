# React example with server side rendering and routing

Clone project, then:

```bash
$ npm i
$ npm start
```

Then you should be able to open your browser at http://localhost:3000
