import path from 'path'

let srcDir = 'src'
let distDir = 'dist'

export default {
  context: path.join(__dirname, srcDir),
  entry: {
    app: './index.js'
  },
  output: {
    path: path.join(__dirname, distDir),
    filename: '[name].js'
  },
  module: {
    loaders: [
      { test: /\.js$/, exclude: /node_modules/, loader: 'babel' }
    ]
  },
  devServer: {
    contentBase: distDir,
    stats: {
      colors: true
    },
    historyApiFallback: true
  }
}
